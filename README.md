# Wavelet Decomposition of Videos


Python packages required for use:
```
numpy
PyWavelets
OpenCV
tqdm
```

Install requirements with pip

```bash
python3 -m pip install -r requirements.txt --user
```
## Example 

A depth of 2 was used here.  Generally speaking the higher depth number, the larger the cells will be in the resulting video

```bash
python3 videoProcess.py test.mp4 --outputFile coal_stack_2.mp4 --depth 2
```

![dept=2](coal.gif "Logo Title Text 1")
