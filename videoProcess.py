#!/usr/bin/python3

import numpy as np
import cv2
import pywt
import argparse
import os
from tqdm import tqdm

# Wavelet decomposition
def w2d(img, mode="shan", level=1):

    # Datatype conversions
    # convert to grayscale
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    # convert to float
    img = np.float32(img) / 255

    # compute coefficients
    coeffs = pywt.wavedec2(img, mode, level=level)

    # Process Coefficients
    coeffs_H = list(coeffs)
    coeffs_H[0] *= 0

    # reconstruction
    img_H = np.uint8(pywt.waverec2(coeffs_H, mode) * 255)
    return cv2.cvtColor(img_H, cv2.COLOR_GRAY2BGR)


def main():
    parser = argparse.ArgumentParser(
        description="Wavelet decomposition of an input video to make some really cool video. "
        "Outputs out.mp4 to current working directory. Video file name cannot have spaces in it. "
    )
    parser.add_argument(
        "vidPath",
        type=str,
        help="Fully qualified path to video file, doesn't have to be mp4.",
    )
    parser.add_argument(
        "--outputFile",
        type=str,
        help="Optional output video file name. Must be .mp4",
        default="out.mp4",
    )
    parser.add_argument(
        "--depth",
        type=int,
        help="depth of decomposition, optimal value will change with video resolution and desired look. Default = 4",
        default=4,
    )
    args = parser.parse_args()

    cwd = os.getcwd()

    video = cv2.VideoCapture(args.vidPath)
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = video.get(cv2.CAP_PROP_FPS)
    frame_count = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    duration = frame_count / fps

    frames = int(duration * fps)

    video_out = cv2.VideoWriter(
        args.outputFile, cv2.VideoWriter_fourcc(*"mp4v"), int(fps), (width, height)
    )
    print("Generating video....")
    for f in tqdm(range(frames)):
        ret, frame = video.read()
        if not ret:
            break
        img = w2d(frame, "db1", args.depth)
        video_out.write(img)

    video.release()
    video_out.release()
    print("Saved video {}".format(args.outputFile))


if __name__ == "__main__":
    main()
